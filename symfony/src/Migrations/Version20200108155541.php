<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200108155541 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE projects DROP FOREIGN KEY FK_5C93B3A49E225B24');
        $this->addSql('ALTER TABLE speakers_classes DROP FOREIGN KEY FK_97046C959E225B24');
        $this->addSql('ALTER TABLE projects_students DROP FOREIGN KEY FK_CEF46E5B1EDE0F55');
        $this->addSql('ALTER TABLE speakers_classes DROP FOREIGN KEY FK_97046C9542AB4241');
        $this->addSql('ALTER TABLE projects_students DROP FOREIGN KEY FK_CEF46E5B1AD8D010');
        $this->addSql('CREATE TABLE classe (id INT AUTO_INCREMENT NOT NULL, speakers_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_8F87BF9642AB4241 (speakers_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, classes_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_2FB3D0EE9E225B24 (classes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_student (project_id INT NOT NULL, student_id INT NOT NULL, INDEX IDX_213DA356166D1F9C (project_id), INDEX IDX_213DA356CB944F1A (student_id), PRIMARY KEY(project_id, student_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speaker (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE classe ADD CONSTRAINT FK_8F87BF9642AB4241 FOREIGN KEY (speakers_id) REFERENCES speaker (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE9E225B24 FOREIGN KEY (classes_id) REFERENCES classe (id)');
        $this->addSql('ALTER TABLE project_student ADD CONSTRAINT FK_213DA356166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_student ADD CONSTRAINT FK_213DA356CB944F1A FOREIGN KEY (student_id) REFERENCES student (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE classes');
        $this->addSql('DROP TABLE projects');
        $this->addSql('DROP TABLE projects_students');
        $this->addSql('DROP TABLE speakers');
        $this->addSql('DROP TABLE speakers_classes');
        $this->addSql('DROP TABLE students');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE9E225B24');
        $this->addSql('ALTER TABLE project_student DROP FOREIGN KEY FK_213DA356166D1F9C');
        $this->addSql('ALTER TABLE classe DROP FOREIGN KEY FK_8F87BF9642AB4241');
        $this->addSql('ALTER TABLE project_student DROP FOREIGN KEY FK_213DA356CB944F1A');
        $this->addSql('CREATE TABLE classes (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE projects (id INT AUTO_INCREMENT NOT NULL, classes_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, UNIQUE INDEX UNIQ_5C93B3A49E225B24 (classes_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE projects_students (projects_id INT NOT NULL, students_id INT NOT NULL, INDEX IDX_CEF46E5B1AD8D010 (students_id), INDEX IDX_CEF46E5B1EDE0F55 (projects_id), PRIMARY KEY(projects_id, students_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE speakers (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, lastname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE speakers_classes (speakers_id INT NOT NULL, classes_id INT NOT NULL, INDEX IDX_97046C9542AB4241 (speakers_id), INDEX IDX_97046C959E225B24 (classes_id), PRIMARY KEY(speakers_id, classes_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE students (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, lastname VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, age INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE projects ADD CONSTRAINT FK_5C93B3A49E225B24 FOREIGN KEY (classes_id) REFERENCES classes (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE projects_students ADD CONSTRAINT FK_CEF46E5B1EDE0F55 FOREIGN KEY (projects_id) REFERENCES projects (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE projects_students ADD CONSTRAINT FK_CEF46E5B1AD8D010 FOREIGN KEY (students_id) REFERENCES students (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speakers_classes ADD CONSTRAINT FK_97046C9542AB4241 FOREIGN KEY (speakers_id) REFERENCES speakers (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speakers_classes ADD CONSTRAINT FK_97046C959E225B24 FOREIGN KEY (classes_id) REFERENCES classes (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE project_student');
        $this->addSql('DROP TABLE speaker');
        $this->addSql('DROP TABLE student');
    }
}
