<?php

namespace App\Form;

use App\Entity\Classe;
use App\Entity\Project;
use App\Entity\Student;
use App\Repository\StudentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('students', EntityType::class, [
                'class' => Student::class,
                'multiple' => true,
                'expanded' => true,
                'query_builder' => function(StudentRepository $st) use ($options){
                    $firstRequest = $st->createQueryBuilder('students')
                        ->leftJoin('students.projects', 's')
                        ->groupBy('students.id')
                        ->having('count(s.id) < 4');
                    $secondRequest = $st->createQueryBuilder('obj')
                        ->where('obj.id in (' . $firstRequest->getDQL() . ')')
                        ->orWhere('obj in (:student)')
                        ->setParameter('student', $options['student']);
                    return $secondRequest;
                },
                'choice_label' => function(Student $student){
                    return $student->getFirstname() . ' ' . $student->getLastname();
                },
            ])
            ->add('classes', EntityType::class, [
                'class' => Classe::class,
                'choice_label' => function(Classe $classe){
                    return $classe->getName();
                }
            ])
            ->add('score', IntegerType::class, [
                'attr' => [
                    'class' => "input",
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'student' => []
        ]);
    }
}
