<?php

namespace App\Form;

use App\Entity\Classe;
use App\Entity\Speaker;
use App\Repository\SpeakerRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClasseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('speakers', EntityType::class, [
                'class' => Speaker::class,
                'placeholder' => "This is a dropdown",
                'required' => false,
                'query_builder' => function(SpeakerRepository $sp) use ($options) {
                    $firstRequest = $sp->createQueryBuilder('speaker')
                        ->leftJoin('speaker.classes', 'p') 
                        ->groupBy('speaker.id')
                        ->having('count(p.id) < 2');
                    $secondRequest = $sp->createQueryBuilder('obj')
                        ->where('obj.id in (' . $firstRequest->getDQL() . ')')
                        ->orWhere('obj in (:speak)')
                        ->setParameter('speak', $options['speaker']);
                        return $secondRequest;
                    
                },
                'choice_label' => function(Speaker $speaker){
                    return $speaker->getFirstname() . ' ' . $speaker->getLastname();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Classe::class,
            'speaker' => []
        ]);
    }
}
