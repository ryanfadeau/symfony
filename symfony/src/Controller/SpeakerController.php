<?php

namespace App\Controller;

use App\Entity\Speaker;
use App\Form\SpeakerType;
use App\Repository\SpeakerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/speaker")
 */
class SpeakerController extends AbstractController
{
    /**
     * @Route("/", name="speaker_index", methods={"GET"})
     */
    public function index(SpeakerRepository $speakerRepository): Response
    {
        return $this->render('speaker/index.html.twig', [
            'speakers' => $speakerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="speaker_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $speaker = new Speaker();
        $form = $this->createForm(SpeakerType::class, $speaker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($speaker);
            $entityManager->flush();

            return $this->redirectToRoute('speaker_index');
        }

        return $this->render('speaker/new.html.twig', [
            'speaker' => $speaker,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="speaker_show", methods={"GET"})
     */
    public function show(Speaker $speaker): Response
    {
        return $this->render('speaker/show.html.twig', [
            'speaker' => $speaker,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="speaker_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Speaker $speaker): Response
    {
        $form = $this->createForm(SpeakerType::class, $speaker);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('speaker_index');
        }

        return $this->render('speaker/edit.html.twig', [
            'speaker' => $speaker,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="speaker_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Speaker $speaker): Response
    {
        if ($this->isCsrfTokenValid('delete'.$speaker->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($speaker);
            $entityManager->flush();
        }

        return $this->redirectToRoute('speaker_index');
    }
}
